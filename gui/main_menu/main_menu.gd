extends Node2D


func _on_play_pressed():
	get_tree().change_scene("res://world/world.tscn")

func _on_settings_pressed():
	get_tree().change_scene("res://gui/settings_menu/settings_menu.tscn")

func _on_about_pressed():
	get_tree().change_scene("res://gui/about_menu/about_menu.tscn")

func _on_quit_pressed():
	get_tree().quit()