extends Node2D


# buttons
onready var button_fullscreen = get_node("buttons/fullscreen")
onready var button_resolution = get_node("buttons/resolution")
onready var dropdown_resolutions = get_node("buttons/resolution/resolutions")
onready var button_borderless = get_node("buttons/borderless")
onready var button_vsync = get_node("buttons/vsync")

# booleans
onready var fullscreen = OS.is_window_fullscreen()
onready var borderless = OS.get_borderless_window()
onready var vsync = OS.is_vsync_enabled()

# numbers
onready var label_debug = get_node("debug")

# vectors
onready var resolution_initial = OS.get_window_size()
onready var resolution = resolution_initial

# builtins
func _ready():
	button_fullscreen.set_text("Fullscreen: " + str(fullscreen))
	button_resolution.set_text("Resolution: " + str(resolution))
	button_borderless.set_text("Borderless: " + str(borderless))
	button_vsync.set_text("Vsync: " + str(vsync))

	for res in settings.resolutions:
		dropdown_resolutions.add_item(str(res))

	set_process(true)

func _process(delta):
	label_debug.set_text("FPS: " + str(OS.get_frames_per_second()) + "\n"
		+ "Fullscreen: " + str(OS.is_window_fullscreen()) + "\n"
		+ "Resolution: " + str(OS.get_window_size()) + "\n"
		+ "Vsync: " + str(OS.is_vsync_enabled()) + "\n"
		+ "Borderless: " + str(OS.get_borderless_window()))

# signals
func _on_fullscreen_toggle_pressed():
	fullscreen = !fullscreen

	# XXX set the initial resolution on fullscreen=true (im not sure if this is needed, this may be nonsense)
	if fullscreen:
		OS.set_window_size(resolution_initial) # TODO find fullscreen resolution

	button_fullscreen.set_text("Fullscreen: " + str(fullscreen))
	OS.set_window_fullscreen(fullscreen)

	print(OS.get_window_size())

func _on_resolution_pressed():
	# "(width, height)" -> Vector2(width, height)
	resolution = dropdown_resolutions.get_text().split(", ", false)
	var width = resolution[0].replace("(", "")
	var height = resolution[1].replace(")", "")
	resolution = Vector2(int(width), int(height))

	# finally apply resolution
	# XXX and set fullscreen to false ??? (fullscreen only works with desktop resolution) ???
	#fullscreen = false
	#button_fullscreen.set_text("Fullscreen: " + str(fullscreen))
	#OS.set_window_fullscreen(false)
	OS.set_window_size(resolution)
	button_resolution.set_text("Resolution: " + str(resolution))

func _on_borderless_pressed(): # XXX does this do anything?
	borderless = !borderless
	OS.set_borderless_window(borderless)
	button_borderless.set_text("Borderless: " + str(borderless))

func _on_vsync_pressed():
	vsync = !vsync
	OS.set_use_vsync(vsync)
	button_vsync.set_text("Vsync: " + str(vsync))

func _on_back_pressed():
	get_tree().change_scene("res://gui/main_menu/main_menu.tscn")