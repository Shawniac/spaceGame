extends Node2D


# numbers
onready var label_fps = get_node("fps")

# builtins
func _ready():
	set_process(true)

func _process(delta):
	label_fps.set_text("FPS: " + str(OS.get_frames_per_second()))

# signals
func _on_back_pressed():
	get_tree().change_scene("res://gui/main_menu/main_menu.tscn")