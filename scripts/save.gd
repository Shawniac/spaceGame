# save.gd
# Saves and loads relevant variables to and from a json file
extends Node


# DEBUG: "res://..." # save in the project's folder
# RELEASE: "usr://..." # save in the user's home folder
const SAVE_PATH = 'res://save.json'
var _settings = {}


func _ready():
    load_game()


func save_game():
    # get relevant data from all persistent nodes
    var nodes_to_save = get_tree().get_nodes_in_group('persistent')
    for node in nodes_to_save:
        save_dict[node.get_path()] = node.save()

    # store to save file
    var save_file = File.new()
    save_file.open(SAVE_PATH, File.WRITE) # overwrite
    save_file.store_line(save_dict.to_json())
    save_file.close()

func load_game():
    # check if there is any save file
    var save_file = File.new()
    if not save_file.file_exists(SAVE_PATH):
        return

    # read the save file
    save_file.open(SAVE_PATH, File.READ)
    var data = {} # TODO can these lines be merged?
    data.parse_json(save_file.get_as_text())

    # load the data into the corresponding nodes
    for node_path in data.keys():
        var node = get_node(node_path)

        for attribute in data[node_path]:
            if attribute == 'pos':
                node.set_pos(Vector2(data[node_path]['pos']['x'],
                                     data[node_path]['pos']['y']))
            else:
                node.set(attribute, data[node_path][attribute])