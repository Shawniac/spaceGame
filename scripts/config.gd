# config.gd
# Stores, saves and loads game Settings in an ini-style file
extends Node


# DEBUG: 'res://...' # save in the project's folder
# RELEASE: 'usr://...' # save in the user's home folder
const SAVE_PATH = 'res://config.cfg'

var _config_file = ConfigFile.new()
var _settings = {
	'audio': {
		'volume_music': -20,
		'volume_sounds': -15
	},
	'video': {
		'resolution_current': Vector2(1280, 720),
		'resolution_default': Vector2(1280, 720)
	}
} setget get_setting, set_setting # TODO find out if this is stupid or not


func _ready():
	save_settings()
	load_settings()


func save_settings():
	print("saving settings to file")
	var value
	for section in _settings.keys():
		for key in _settings[section]:
			value = _settings[section][key]
			_config_file.set_value(section, key, value)

	_config_file.save(SAVE_PATH)

func load_settings():
	print("loading settings from file")
	# try to load from file
	var error = _config_file.load(SAVE_PATH)
	# check if the loading was successful
	if error != OK:
		return [] # return an empty array if loading failed

	# retrieve the config values from the save file and save them into _settings
	var value
	for section in _settings.keys():
		for key in _settings[section]:
			value = _config_file.get_value(section, key, null)
			_settings[section][key] = value


func get_setting(category, key):
	return _settings[category][key]

func set_setting(category, key, value):
	_settings[category][key] = value