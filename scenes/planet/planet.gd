extends Area2D


export(String, "Mars", "Earth", "Moon", "Venus", "Jupiter", "Uranus", "Pluto") var name = "Earth" setget set_name


onready var sprite = get_node("sprite")


func _ready():
	pass


func set_name(new_name):
	name = new_name
	# sprite.set_texture() # TODO make Planet Sprite selectable