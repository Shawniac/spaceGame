tool
extends Node2D


onready var ship = get_parent().get_parent()
onready var thruster = get_node("thruster")
export(String, "EMPTY", "COCKPIT", "SHIELDS", "CARGO-BAY", "ENGINE") var type = "EMPTY" setget set_type
var health = -1

#signal module_pressed


func _ready():
	print("module '" + type + "' ready")
	health = 5

func _on_area2d_input_event( viewport, event, shape_idx ):
	# if a module is pressed with the left mouse button
	# TODO make this multiplatform aka touch+mousebutton
	# XXX what to do with a gamepad
	if event.type == InputEvent.MOUSE_BUTTON \
			and event.button_index == BUTTON_LEFT \
			and event.is_pressed():
		# emit_signal(module_pressed, get_node("."))
		var dmg = 1 # TODO randomize damage: randi() % (min_val-max_val-1) + min_val) # random int in [min_val, max_val] inclusive
		if health > 0:
			health -= dmg
			ship.health -= dmg
			var status = ""
			if health <= 0:
				status = "broken"
				# TODO play destroy animation here
				get_node("sprite").set_modulate(Color(0.95, 0.1, 0.1))
				thruster.get_node("sprite").set_modulate(Color(0.95, 0.1, 0.1))
				thruster.get_node("flames").hide()
			else:
				status = str(health)
	
			get_node("sounds").play("damage")
			print(str(type) + " status: " + status)



func set_type(new_type):
	print("type: " + type + " new_type: " + new_type)
	type = new_type

	#if type == "THRUSTER":
	#	thruster.show()
	#elif thruster.is_visible():
	#	thruster.hide()