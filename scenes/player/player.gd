# player.gd
# Player logic here
extends Node2D
# TODO update ship health on module damage

var health = -1
var speed = -1

var module_count = -1
var module_health = -1


func _ready():
	print("ship ready")
	module_count = 4
	module_health = 5
	# TODO for module in modules: health += module.health
	health = module_count * module_health
	speed = 1
	add_to_group('persistent')


func save():
	var save_dict = {
		pos = {
			x = get_pos().x,
			y = get_pos().y
		},
		speed = speed
	}

	return save_dict