extends Node2D


# objects
onready var ship = get_node("ship")
onready var flames = get_node("ship/modules/engine/thruster/flames")
onready var planet_a = get_node("planet a")
onready var planet_b = get_node("planet b")
var target = null

# labels
onready var label_fps = get_node("gui/fps")
onready var label_time = get_node("gui/time")
onready var label_health = get_node("gui/health")
onready var label_thruster_status = get_node("gui/thruster-status")

# numbers
var time = -1
var health = -1
var distance = -1

# vectors
var difference = Vector2()
var velocity = Vector2()

# builtins
func _ready():
	time = 0
	set_process(true)
	set_process_input(true)

func _process(delta):
	time += delta
	label_time.set_text("Time: " + str(int(time)) + "s")

	label_fps.set_text("FPS: " + str(OS.get_frames_per_second()))

	# TODO update health label only on damage taken
	health = ship.health
	label_health.set_text("Health: " + str(health))

	# TODO determine better thruster status indicator than flames.is_visible()
	if target != null and flames.is_visible():
		distance = ship.get_pos().distance_to(target.get_pos())
		if distance > 16:
			difference = target.get_pos() - ship.get_pos()
			velocity = difference * ship.speed * delta

			ship.set_pos(ship.get_pos() + velocity)
			ship.set_rot(difference.angle() - PI/2)
		else:
			print("target reached")
			target = null

	var status = "-1"
	if flames.is_visible():
		status = "working"
	else:
		status = "broken"
	label_thruster_status.set_text("Thrusters: " + status)

func _input(event):
#	if event.type == InputEvent.MOUSE_BUTTON \
#	and event.is_pressed() \
#	and event.button_index == BUTTON_LEFT:
#		print("left click registered at: " + str(event.pos))

	if event.is_action_pressed("ui_cancel"):
		print("escape pressed")
		get_tree().quit()

# signals
func _on_planet_a_input_event( viewport, event, shape_idx ):
	if event.type == InputEvent.MOUSE_BUTTON \
	and event.is_pressed() \
	and event.button_index == BUTTON_LEFT:
		print("planet a clicked")
		target = planet_a

func _on_planet_b_input_event( viewport, event, shape_idx ):
	if event.type == InputEvent.MOUSE_BUTTON \
	and event.is_pressed() \
	and event.button_index == BUTTON_LEFT:
		print("planet b clicked")
		target = planet_b