[//]: # "TODO include spaceGame build-status here"
![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg) This is just a placeholder.

# What is this repository?

The official home of ***spaceGame*** (placeholder), which was made with [Godot]

# Resources
## Tools

### Asesprite (pixel art, linux support, good for animation)
https://www.aseprite.org/
### GIMP (good gfx tool overall, resizing, cropping, etc.)
https://www.gimp.org/
### Godot Engine (game engine, FOSS, no fees whatsoever)
https://godotengine.org/
### KeeWeb (secure storage for passwords, etc.)
https://keeweb.info/
### PixelEdit (pixel art, no linux support, good for tilesets)
http://pyxeledit.com/
### VSCode (coding, managing text-based files)
https://code.visualstudio.com/
### Wire (secure communication and file sharing)
https://wire.com/en/

## Websites

### ChooseALicense (chosing a license, d'oh)
https://choosealicense.com/
### Reddit (reaching out to both gamers and gamedevs, find news and resources for gamedev)
https://www.reddit.com/

a compiled list of youtube-links on godot:

https://www.reddit.com/r/gamedev/comments/6hklf6/as_a_veteran_designer_and_teaching_on_the_side_i/diztcpq/
### TLDRLegal (learning about licenses, and chosing one)
https://tldrlegal.com/
### YouTube (tutorials and promotion)
https://www.youtube.com/


[ci]: https://about.gitlab.com/gitlab-ci/
[documentation]: https://gohugo.io/overview/introduction/
[godot]: https://godotengine.org/
[hugo]: https://gohugo.io/
[install]: https://gohugo.io/overview/installing/
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains/
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages/